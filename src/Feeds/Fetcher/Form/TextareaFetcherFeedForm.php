<?php

namespace Drupal\feedstextareafetcher\Feeds\Fetcher\Form;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Feeds\Fetcher\Form\UploadFetcherFeedForm;
use Drupal\feeds\Plugin\Type\Parser\ParserWithTemplateInterface;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form on the feed edit page for the TextareaFetcher.
 */
class TextareaFetcherFeedForm extends UploadFetcherFeedForm {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file repository.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $fetcher_feed_form = parent::create($container);
    $fetcher_feed_form->setFileSystem($container->get('file_system'));
    $fetcher_feed_form->setFileRepository($container->get('file.repository'));
    $fetcher_feed_form->setMessenger($container->get('messenger'));
    return $fetcher_feed_form;
  }

  /**
   * Sets the file system service.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  protected function setFileSystem(FileSystemInterface $file_system) {
    $this->fileSystem = $file_system;
  }

  /**
   * Sets the file repository.
   *
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository.
   */
  protected function setFileRepository(FileRepositoryInterface $file_repository) {
    $this->fileRepository = $file_repository;
  }

  /**
   * Sets the messenger service.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  protected function setMessenger(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, ?FeedInterface $feed = NULL) {
    $form['source'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text'),
      '#description' => $this->t('Paste text here.'),
      '#required' => TRUE,
    ];

    if (!$feed instanceof FeedInterface) {
      return $form;
    }

    // Add contents of existing file as default value, if there is one.
    $feed_config = $feed->getConfigurationFor($this->plugin);
    if (!empty($feed_config['fid'])) {
      $existing_file = $this->fileStorage->load($feed_config['fid']);
      if ($existing_file) {
        $uri = $existing_file->getFileUri();
        if (file_exists($uri) && is_readable($uri)) {
          $form['source']['#default_value'] = file_get_contents($uri);
        }
        else {
          $this->messenger->addWarning($this->t('The previously saved source, saved as %filepath, is not readable.', [
            '%filepath' => $uri,
          ]));
        }
      }
    }
    else {
      // Alternatively, if the selected parser supports templates, use that as
      // default.
      // @todo add test coverage for this.
      $parser = $feed->getType()->getParser();
      if ($parser instanceof ParserWithTemplateInterface) {
        $form['source']['#default_value'] = $parser->getTemplateContents($feed->getType(), $feed);
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state, ?FeedInterface $feed = NULL) {
    $feed_config = $feed->getConfigurationFor($this->plugin);

    // Check if there is an existing file.
    if (!empty($feed_config['fid'])) {
      $existing_file = $this->fileStorage->load($feed_config['fid']);
      if ($existing_file) {
        $uri = $existing_file->getFileUri();
        // Check if the contents has been changed.
        if (file_exists($uri) && is_readable($uri) && sha1($form_state->getValue('source')) === sha1_file($uri)) {
          // Source did not change. Just only make sure the source url is saved
          // on the feed entity.
          $feed->setSource($existing_file->getFileUri());
          return;
        }
      }
    }

    // Generate a UUID that maps to this feed for file usage. We can't depend
    // on the feed id since this could be called before an id is assigned.
    $feed_config['usage_id'] = $feed_config['usage_id'] ?: $this->uuid->generate();

    // Compose file name.
    $name = $feed_config['usage_id'] . '.txt';
    $directory = $this->plugin->getConfiguration('directory');
    if (substr($directory, strlen($directory)) != '/') {
      $directory .= '/';
    }

    // Delete the old file, if there is one.
    if (!empty($feed_config['fid'])) {
      $this->deleteFile($feed_config['fid'], $feed_config['usage_id']);
    }

    // Save source text as new file.
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $file = $this->fileRepository->writeData($form_state->getValue('source'), $directory . $name);

    // Add usage to the new file.
    $this->fileUsage->add($file, 'feeds', $this->plugin->pluginType(), $feed_config['usage_id']);
    $file->save();

    // And save the feed config.
    $feed_config['fid'] = $file->id();
    $feed->setSource($file->getFileUri());

    $feed->setConfigurationFor($this->plugin, $feed_config);
  }

  /**
   * Deletes a file.
   *
   * @param int $file_id
   *   The file id.
   * @param string $uuid
   *   The file UUID associated with this file.
   */
  protected function deleteFile($file_id, $uuid) {
    if ($file = $this->fileStorage->load($file_id)) {
      $this->fileUsage->delete($file, 'feeds', $this->plugin->pluginType(), $uuid);
      $file->delete();
    }
  }

}
