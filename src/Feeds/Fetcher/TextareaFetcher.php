<?php

namespace Drupal\feedstextareafetcher\Feeds\Fetcher;

use Drupal\feeds\Feeds\Fetcher\UploadFetcher;

/**
 * Defines a textarea fetcher.
 *
 * @FeedsFetcher(
 *   id = "textarea",
 *   title = @Translation("Textarea"),
 *   description = @Translation("Copy past content in a textarea field."),
 *   form = {
 *     "configuration" = "Drupal\feedstextareafetcher\Feeds\Fetcher\Form\TextareaFetcherForm",
 *     "feed" = "Drupal\feedstextareafetcher\Feeds\Fetcher\Form\TextareaFetcherFeedForm",
 *   },
 * )
 */
class TextareaFetcher extends UploadFetcher {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $schemes = $this->getSchemes();
    $scheme = in_array('private', $schemes) ? 'private' : reset($schemes);

    return [
      'directory' => $scheme . '://feeds',
    ];
  }

  /**
   * Deletes a file.
   *
   * @param int $file_id
   *   The file id.
   * @param string $uuid
   *   The file UUID associated with this file.
   *
   * @see file_delete()
   */
  protected function deleteFile($file_id, $uuid) {
    if ($file = $this->fileStorage->load($file_id)) {
      $this->fileUsage->delete($file, 'feeds', $this->pluginType(), $uuid);
      $file->delete();
    }
  }

}
