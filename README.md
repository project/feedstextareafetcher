Feeds Textarea Fetcher
======================
This module adds a fetcher plugin which allows you to enter the source directly
in a textarea when creating a feed.

How it works
------------
The textarea fetcher extends the Upload fetcher of Feeds, so it inherits a lot
of functionality from that fetcher.
When adding or editing a feed with this fetcher, a text box appears in which a
user can paste (or type) their text source.

The source text gets saved as a file on the filesystem with the 'txt' extension.
It is this file that gets read when an import is performed, but also when
editing a feed.
On the feed type configuration, a directory can be specified in which the saved
file will get stored.
