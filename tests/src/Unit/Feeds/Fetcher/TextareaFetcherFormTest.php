<?php

namespace Drupal\Tests\feedstextareafetcher\Unit\Feeds\Fetcher\Form;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Tests\feeds\Unit\FeedsUnitTestCase;
use Drupal\feeds\Plugin\Type\FeedsPluginInterface;
use Drupal\feedstextareafetcher\Feeds\Fetcher\Form\TextareaFetcherForm;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @coversDefaultClass \Drupal\feedstextareafetcher\Feeds\Fetcher\Form\TextareaFetcherForm
 * @group feedstextareafetcher
 */
class TextareaFetcherFormTest extends FeedsUnitTestCase {

  use ProphecyTrait;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->fileSystem = $this->prophesize(FileSystemInterface::class);
    $this->streamWrapperManager = $this->prophesize(StreamWrapperManagerInterface::class);
  }

  /**
   * Builds container with services.
   *
   * @return \Drupal\Core\DependencyInjection\ContainerBuilder
   *   The container builder.
   */
  protected function buildContainer() {
    $container = new ContainerBuilder();
    \Drupal::setContainer($container);

    $container->set('file_system', $this->fileSystem->reveal());
    $container->set('stream_wrapper_manager', $this->streamWrapperManager->reveal());
    $container->set('string_translation', $this->getStringTranslationStub());

    return $container;
  }

  /**
   * Tests the configuration form.
   *
   * @covers ::buildConfigurationForm
   * @covers ::validateConfigurationForm
   */
  public function testConfigurationForm() {
    $this->fileSystem->prepareDirectory(Argument::type('string'), Argument::type('int'))->willReturn(TRUE);
    $this->streamWrapperManager->getWrappers(StreamWrapperInterface::WRITE_VISIBLE)->willReturn([]);

    $container = $this->buildContainer();

    // Create form object.
    $form_object = TextareaFetcherForm::create($container);
    $plugin = $this->prophesize(FeedsPluginInterface::class);
    $form_object->setPlugin($plugin->reveal());

    // Create form state.
    $form_state = new FormState();

    // Build form.
    $form = $form_object->buildConfigurationForm([], $form_state);
    $form['directory']['#parents'] = ['directory'];

    // Validate.
    $form_state->setValue(['directory'], 'vfs://feeds/uploads');
    $form_state->setValue(['allowed_extensions'], 'csv');

    $form_object->validateConfigurationForm($form, $form_state);
    $this->assertCount(0, $form_state->getErrors());
  }

  /**
   * Tests the configuration form where folder creation failed.
   *
   * @covers ::buildConfigurationForm
   * @covers ::validateConfigurationForm
   */
  public function testFolderCreationFailure() {
    $this->fileSystem->prepareDirectory(Argument::type('string'), Argument::type('int'))->willReturn(FALSE);
    $this->streamWrapperManager->getWrappers(StreamWrapperInterface::WRITE_VISIBLE)->willReturn([]);

    $container = $this->buildContainer();

    // Create form object.
    $form_object = TextareaFetcherForm::create($container);
    $plugin = $this->prophesize(FeedsPluginInterface::class);
    $form_object->setPlugin($plugin->reveal());

    // Create form state.
    $form_state = new FormState();

    // Build form.
    $form = $form_object->buildConfigurationForm([], $form_state);
    $form['directory']['#parents'] = ['directory'];

    // Validate.
    $form_state->setValue(['directory'], 'vfs://noroot');
    $form_object->validateConfigurationForm($form, $form_state);
    $this->assertSame('The chosen directory does not exist and attempts to create it failed.', (string) $form_state->getError($form['directory']));
  }

}
