<?php

namespace Drupal\Tests\feedstextareafetcher\Functional\Feeds\Fetcher\Form;

use Drupal\Tests\feeds\Functional\FeedsBrowserTestBase;

/**
 * @coversDefaultClass \Drupal\feedstextareafetcher\Feeds\Fetcher\Form\TextareaFetcherFeedForm
 * @group feedstextareafetcher
 */
class TextareaFetcherFeedFormTest extends FeedsBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'feeds',
    'node',
    'user',
    'file',
    'filter',
    'feedstextareafetcher',
    'feedstextareafetcher_test',
  ];

  /**
   * Tests that parser template contents are used inside the textarea.
   */
  public function testViewCsvTemplate() {
    // Add body field to article content type.
    node_add_body_field($this->nodeType);

    // Create a feed type.
    $feed_type = $this->createFeedTypeForCsv([
      'guid' => 'guid',
      'title' => 'title',
      'body' => 'body',
    ], [
      'fetcher' => 'textarea',
      'fetcher_configuration' => [
        'directory' => 'public://feeds',
      ],
      'processor_configuration' => [
        'values' => [
          'type' => 'article',
        ],
      ],
      'mappings' => array_merge($this->getDefaultMappings(), [
        [
          'target' => 'body',
          'map' => ['value' => 'body'],
          'settings' => ['format' => 'plain_text'],
        ],
      ]),
    ]);

    $this->drupalGet('feed/add/' . $feed_type->id());

    // Assert that the template contents are displayed inside the textarea.
    $this->assertSession()->fieldValueEquals('plugin[fetcher][source]', 'guid,title,body');
  }

  /**
   * Tests viewing template of a dummy parser.
   */
  public function testViewDummyParserTemplate() {
    $feed_type = $this->createFeedType([
      'fetcher' => 'textarea',
      'fetcher_configuration' => [
        'directory' => 'public://feeds',
      ],
      'parser' => 'feedstextareafetcher_test_dummy_parser_with_template',
      'mappings' => [
        [
          'target' => 'title',
          'map' => [
            'value' => 'parent:title',
          ],
        ],
      ],
    ]);

    $this->drupalGet('feed/add/' . $feed_type->id());

    // Assert that the template contents are displayed inside the textarea.
    $this->assertSession()->fieldValueEquals('plugin[fetcher][source]', 'The quick brown fox jumps over the lazy dog');
  }

}
