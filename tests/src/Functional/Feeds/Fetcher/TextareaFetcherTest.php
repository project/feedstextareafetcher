<?php

namespace Drupal\Tests\feedstextareafetcher\Functional\Feeds\Fetcher;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Tests\feeds\Functional\FeedsBrowserTestBase;
use Drupal\feeds\Entity\Feed;
use Drupal\feeds\Plugin\Type\Processor\ProcessorInterface;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;

/**
 * @coversDefaultClass \Drupal\feedstextareafetcher\Feeds\Fetcher\TextareaFetcher
 * @group feedstextareafetcher
 */
class TextareaFetcherTest extends FeedsBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'feeds',
    'node',
    'user',
    'file',
    'filter',
    'feedstextareafetcher',
  ];

  /**
   * The feed type entity.
   *
   * @var \Drupal\feeds\Entity\FeedType
   */
  protected $feedType;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Add body field to article content type.
    node_add_body_field($this->nodeType);

    // Create a feed type.
    $this->feedType = $this->createFeedTypeForCsv([
      'guid' => 'guid',
      'title' => 'title',
      'body' => 'body',
    ], [
      'fetcher' => 'textarea',
      'fetcher_configuration' => [
        'directory' => 'public://feeds',
      ],
      'processor_configuration' => [
        'authorize' => FALSE,
        'update_existing' => ProcessorInterface::UPDATE_EXISTING,
        'values' => [
          'type' => 'article',
        ],
      ],
      'mappings' => array_merge($this->getDefaultMappings(), [
        [
          'target' => 'body',
          'map' => ['value' => 'body'],
          'settings' => ['format' => 'plain_text'],
        ],
      ]),
    ]);

    $this->fileSystem = $this->container->get('file_system');
  }

  /**
   * Tests importing a new feed.
   */
  public function testImportNewFeed() {
    // Create feed and import.
    $edit = [
      'title[0][value]' => $this->randomMachineName(),
      'plugin[fetcher][source]' => file_get_contents($this->resourcesPath() . '/csv/content.csv'),
    ];
    $this->drupalGet('feed/add/' . $this->feedType->id());
    $this->submitForm($edit, 'Save and import');

    // Load feed.
    $feed = Feed::load(1);

    // Assert that 2 nodes have been created.
    static::assertEquals(2, $feed->getItemCount());
    $this->assertNodeCount(2);
  }

  /**
   * Test submitting the feed form without changing anything.
   */
  public function testImportExistingFeed() {
    // Create a file with contents.
    $directory = 'public://feeds';
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $file = $this->container->get('file.repository')->writeData(file_get_contents($this->resourcesPath() . '/csv/content.csv'), $directory . '/feed.csv');

    // Create a feed, point to the created file.
    $feed = $this->createFeed($this->feedType->id(), [
      'config' => [
        'fetcher' => [
          'fid' => $file->id(),
        ],
      ],
    ]);

    // Post edit form and import.
    $this->drupalGet('feed/' . $feed->id() . '/edit');
    $this->submitForm([], 'Save and import');

    // Assert that 2 nodes have been created.
    $this->assertNodeCount(2);
  }

  /**
   * Tests submitting the feed form with new contents.
   */
  public function testImportChangedFeed() {
    // Create a feed and perform an import.
    $directory = 'public://feeds';
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $file = \Drupal::service('file.repository')->writeData(file_get_contents($this->resourcesPath() . '/csv/content.csv'), $directory . '/feed.csv');
    $feed = $this->createFeed($this->feedType->id(), [
      'source' => $directory . '/feed.csv',
      'config' => [
        'fetcher' => [
          'fid' => $file->id(),
          'usage_id' => 'foo',
        ],
      ],
    ]);
    $feed->import();

    // Assert that 2 nodes have been created.
    $this->assertNodeCount(2);

    // Now import changed contents.
    $edit = [
      'plugin[fetcher][source]' => file_get_contents($this->resourcesPath() . '/csv/content_updated.csv'),
    ];
    $this->drupalGet('feed/' . $feed->id() . '/edit');
    $this->submitForm($edit, 'Save and import');

    // Assert that the old file no longer exists and that a new one is saved.
    $this->assertFalse(file_exists('public://feeds/feed.csv'));
    $this->assertTrue(file_exists('public://feeds/foo.txt'));

    // Assert that 2 nodes have changed.
    $expected_body = [
      1 => 'Lorem ipsum dolor sit amet.',
      2 => 'Ut wisi enim ad minim veniam.',
    ];
    foreach ($expected_body as $nid => $value) {
      $node = Node::load($nid);
      $this->assertEquals($value, $node->body->value);
    }
  }

  /**
   * Tests that the file is removed when the feed is deleted.
   */
  public function testRemoveFileWhenDeletingFeed() {
    // Create a feed first.
    $edit = [
      'title[0][value]' => $this->randomMachineName(),
      'plugin[fetcher][source]' => file_get_contents($this->resourcesPath() . '/csv/content.csv'),
    ];
    $this->drupalGet('feed/add/' . $this->feedType->id());
    $this->submitForm($edit, 'Save');

    // Assert that a file was created.
    $feed = Feed::load(1);
    $feed_config = $feed->getConfigurationFor($this->feedType->getFetcher());
    $this->assertTrue(file_exists('public://feeds/' . $feed_config['usage_id'] . '.txt'));

    // Now delete the feed and assert the file no longer exists.
    $feed->delete();
    $this->assertFalse(file_exists('public://feeds/' . $feed_config['usage_id'] . '.txt'));
  }

  /**
   * Tests that an import fails gracefully when the file to import is removed.
   */
  public function testImportFail() {
    // Create a feed.
    $edit = [
      'title[0][value]' => $this->randomMachineName(),
      'plugin[fetcher][source]' => file_get_contents($this->resourcesPath() . '/csv/content.csv'),
    ];
    $this->drupalGet('feed/add/' . $this->feedType->id());
    $this->submitForm($edit, 'Save');

    // Remove the file manually.
    $feed = Feed::load(1);
    $feed_config = $feed->getConfigurationFor($this->feedType->getFetcher());
    $file = File::load($feed_config['fid']);
    $file->delete();

    // Try an import and assert that it fails without any AJAX errors.
    $this->drupalGet('feed/1/import');
    $this->submitForm([], 'Import');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Resource is not a file');
  }

  /**
   * Tests that a warning is displayed when the file to import no longer exists.
   */
  public function testFeedFormWithRemovedFile() {
    // Create a feed.
    $edit = [
      'title[0][value]' => $this->randomMachineName(),
      'plugin[fetcher][source]' => file_get_contents($this->resourcesPath() . '/csv/content.csv'),
    ];
    $this->drupalGet('feed/add/' . $this->feedType->id());
    $this->submitForm($edit, 'Save');

    // Remove the file on the filesystem manually, but keep the file entity.
    $feed = Feed::load(1);
    $feed_config = $feed->getConfigurationFor($this->feedType->getFetcher());
    $file = File::load($feed_config['fid']);
    $uri = $file->getFileUri();
    $this->container->get('file_system')->delete($uri);

    // Assert that a message is displayed about the missing file on the feed
    // form.
    $this->drupalGet('feed/1/edit');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(sprintf('The previously saved source, saved as %s, is not readable.', $uri));
  }

}
