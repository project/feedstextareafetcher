<?php

namespace Drupal\feedstextareafetcher_test\Feeds\Parser;

use Drupal\feeds\FeedInterface;
use Drupal\feeds\FeedTypeInterface;
use Drupal\feeds\Feeds\Parser\ParserBase;
use Drupal\feeds\Plugin\Type\Parser\ParserWithTemplateInterface;
use Drupal\feeds\Result\FetcherResultInterface;
use Drupal\feeds\Result\ParserResult;
use Drupal\feeds\StateInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Defines a parser with template.
 *
 * @FeedsParser(
 *   id = "feedstextareafetcher_test_dummy_parser_with_template",
 *   title = "Dummy parser with template (ftaf)",
 *   description = @Translation("Dummy parser"),
 * )
 */
class DummyParserWithTemplate extends ParserBase implements ParserWithTemplateInterface {

  /**
   * {@inheritdoc}
   */
  public function parse(FeedInterface $feed, FetcherResultInterface $fetcher_result, StateInterface $state) {
    return new ParserResult();
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingSources() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplate(FeedTypeInterface $feed_type, ?FeedInterface $feed = NULL): Response {
    $headers = [
      'Cache-Control' => 'max-age=60, must-revalidate',
      'Content-Disposition' => 'attachment; filename="foo.txt"',
      'Content-type' => "text/plain; charset=utf-8",
    ];

    return new Response($this->getTemplateContents($feed_type, $feed), 200, $headers);
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplateContents(FeedTypeInterface $feed_type, ?FeedInterface $feed = NULL): string {
    return 'The quick brown fox jumps over the lazy dog';
  }

}
